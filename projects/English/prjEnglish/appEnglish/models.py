from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime
from django.utils import timezone
import uuid
from mixer.backend.django import Mixer



class User2(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    
    GENERATED = 'GN'
    AVAILABLE    = 'AV'
    ACTIVATED    = 'AC'
    DELETED      = 'DL'
    BLOCKED      = 'BL'
    LICENSE_STATUS=(
        (GENERATED, 'Generated'),
        (AVAILABLE, 'Available'),
        (ACTIVATED, 'Activated'),
        (BLOCKED, 'Blocked'),
        (DELETED, 'Deleted'),
    )
    status  = models.CharField("Status",
                               help_text="License status", 
                               choices=LICENSE_STATUS,
                               default=GENERATED,
                               max_length = 2
                               )
    downloadtime= models.DateTimeField(null = True,
                                       blank = True
                                       )
    activatetime= models.DateTimeField(null = True,
                                       blank = True
                                       )
    textpassword = models.CharField("Text password",
                                help_text="Text password",
                                max_length=256,
                                null = False,
                                blank = True,
                                default = ""
                                )
    additionalinfo  = models.TextField("Additional information",
                                       help_text="Additional information",
                                       null = False,
                                       blank = True,
                                       default = ""
                                       )



@receiver(post_save, sender=User)
def user2create(instance, **kwargs):
    if hasattr(instance, 'user2') == False:
           us2 = User2(user = instance) 
           us2.save()







#===============================================================================
# class LicenseHistory(models.Model):
#     GENERATED = 'NA'
#     AVAILABLE    = 'AV'
#     ACTIVATED    = 'AC'
#     DELETED      = 'DL'
#     BLOCKED      = 'BL'
#     LICENSE_STATUS=(
#         (GENERATED, 'Generated'),
#         (AVAILABLE, 'Available'),
#         (ACTIVATED, 'Activated'),
#         (BLOCKED, 'Blocked'),
#         (DELETED, 'Deleted'),
#     )
#     license = models.ForeignKey(User2,on_delete=models.PROTECT)
#     success = models.BooleanField()
#     statusafter  = models.PositiveSmallIntegerField("Status after changes",
#                                                     help_text="License status after action",
#                                                     choices=LICENSE_STATUS,
#                                                     default=GENERATED)
#     actiondatetime = models.DateTimeField("Date/time of action",
#                                           help_text="Date/time of action",
#                                           auto_now_add=True)
#     ip = models.GenericIPAddressField("IP address"),
#     additionalinfo  = models.TextField("Additional information",
#                                        help_text="Additional information")
#===============================================================================
    

#===============================================================================
# @receiver(post_save, sender=User2)
# def user2save(instance, **kwargs):
#     make_password
#     us = GoodsLog(
#         goods=instance,
#         goods_price=instance.good_price,
#         goods_time=timezone.now())
#     gl.save()
#===============================================================================

    
    #class Users(models.Model):
#        GENERATED = 'NA'
#        AVAILABLE    = 'AV'
#        ACTIVATED    = 'AC'
#        DELETED      = 'DL'
#        BLOCKED      = 'BL'
#        LICENSE_STATUS=(
#            (GENERATED, 'Generated'),
#            (AVAILABLE, 'Available'),
#            (ACTIVATED, 'Activated'),
#            (BLOCKED, 'Blocked'),
#            (DELETED, 'Deleted'),
#            )
#        USER = 'USR'
#        ADMINISTRATOR  = 'ADM'
#        USER_ROLE=(
#            (USER, 'USER'),
#            (ADMINISTRATOR, 'ADMINISTRATOR'),
#            )
#        def logingenerate():
#            mixer = Mixer(fake=False)
#            IsUnique = False
#            while IsUnique = False:
#                try:
#                    usertemp = mixer.blend(models.CharField, name=mixer.RANDOM)
#                    Users.objects.get(login=usertemp)
#                except Entry.DoesNotExist:
#                    IsUnique = True
#                except MultipleObjectsReturned:
#                    IsUnique = False:   
#            return usertemp
#
#        def passwordgenerate():
#            mixer = Mixer(fake=False)
#            IsUnique = False
#            while IsUnique = False:
#                try:
#                    pwdtemp = mixer.blend(models.CharField, name=mixer.RANDOM)
#                    Users.objects.get(password=pwdtemp)
#                except Entry.DoesNotExist:
#                    IsUnique = True
#                except MultipleObjectsReturned:
#                    IsUnique = False:   
#            return pwdtemp
#            
#    
#    #license = models.ForeignKey("License",
#    #                             License, on_delete=models.PROTECT)
#        login  = models.CharField("Login for app download",
#                                  help_text="License status after action",
#                                  max_length=256,
#                                  unique = True,
#                                  default=logingenerate)
#        password = models.CharField("Password",
#                                    help_text="License status after action",
#                                    max_length=256,
#                                    unique = True,
#                                    default=passwordgenerate)
#        role  = models.PositiveSmallIntegerField("Status"help_text="License status",
#                                                 choices=USER_ROLE,
#                                                 default=USER)
#     
#        alloweddownloads = models.IntegerField("Number of allowed downloads",
#                                               "Number of allowed downloads for this login",
#                                               default = 1)
#        downloadscounter = models.IntegerField("Number of completed downloads",
#                                               "Number of  successfully completed downloads",
#                                               default = 1)
#    
#
#    class UsersHistory(models.Model):
#        appdownloadloginpassword = models.ForeignKey(Users,
#                                                     on_delete=models.PROTECT
#                                                     )
#        actiondatetime = models.DateTimeField("Download date/time",
#                                          help_text="Date/time of App download",
#                                          auto_now_add=True)
#        success = models.BooleanField()
#        ip = models.GenericIPAddressField("IP address"),
#        additionalinfo  = models.TextField("Additional information",
#                                           help_text="Additional information")
#    
#    
#    