from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from appEnglish.models import User2

# Register your models here.


#admin.site.register(User2)
#admin.site.register(LicenseHistory)





# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class UserInline(admin.StackedInline):
    model = User2
    can_delete = False
    verbose_name_plural = 'User(donwloads)'

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (UserInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)