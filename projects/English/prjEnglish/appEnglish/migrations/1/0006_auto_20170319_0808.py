# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-19 08:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appEnglish', '0005_auto_20170319_0803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user2',
            name='downloadtime',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='user2',
            name='textpassword',
            field=models.CharField(blank=True, default='', help_text='Text password', max_length=256, verbose_name='Text password'),
        ),
    ]
