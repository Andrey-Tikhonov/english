from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader, Context
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.template.context_processors import csrf
from appEnglish.models import User2
from django.contrib.auth.models import User
from annoying.decorators import render_to
from django.contrib import auth
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
#from mixer.backend.django import Mixer
from django.conf import settings
import uuid
from random import choice
from string import ascii_letters, digits

#from prgEnglish.settings import STATIC_ROOT




@csrf_protect
@login_required
@render_to('download.html')
def download(request):
        return {}
    

@csrf_protect
@login_required
def startdownload(request):
    with open(settings.STATIC_ROOT+'app.apk', 'rb') as file: 
        response = HttpResponse(file.read(), content_type='application/vnd.android.package-archive')
#        response['Content-Length'] = str(os.path.getsize(settings.STATIC_ROOT+'app.apk'))
        response['Content-Disposition'] = 'attachment; filename="app.apk"'
        request.user.is_active=False
        request.user.save()
        if hasattr(request.user, 'user2') == False:
           us2 = User2(user = request.user) 
           us2.save()
        us2=User2.objects.get(user = request.user)   
        us2.downloadtime = timezone.now()
#        print(us2.downloadtime)
        us2.save()
        request.user.save()
        logout(request)
        return response



def passwordgenerate(length = 8, strong = False):
    rid = ''
    for x in range(length):
        rid += choice(('!@#$%^&*()_-+=' if strong else '') + ascii_letters + digits)
    return rid



@csrf_exempt
def recordactivate(request, lic = None ):

    print(request.method)
    print(lic)

    print(request.POST['pwd'])
    print(request.body)
    r = HttpResponse("")
    r.status_code = 501    
    if request.method == 'POST'and 'pwd' in request.POST and lic is not None:
        try:
            us = User.objects.filter(username =  lic).get(user2__textpassword=request.POST['pwd'])
            if us.is_active:
                r.status_code = 401
                return r
            us2=User2.objects.get(user = us)
            if us2.downloadtime is not None:
                us2.activatetime= timezone.now()
                us2.status = 'AC'
                us2.save()
                r.status_code = 200
                return r
            else:
                r.status_code = 403
                return r
        except User.DoesNotExist:
            r.status_code = 404
            return r
        except User.MultipleObjectsReturned:
            r.status_code = 500               
            return r
    r.status_code = code   
    return r

    

@login_required
@render_to('license.html')
def recordadd(request):
    page = request.GET.get('page')
    pwd = passwordgenerate(strong = True)
    usr = str(uuid.uuid4())
    #---------------------------------------------------------------- print(usr)
    #---------------------------------------------------------------- print(pwd)
    u = User(username=usr,is_superuser=0)
    u.set_password(pwd)
    u.save()
    us2=User2.objects.get(user = u)
    us2.textpassword= pwd
    us2.save()
    return HttpResponseRedirect('/license/')

 

@login_required
@render_to('license.html')
def recordinfo(request):
    return HttpResponseRedirect('/license/')

@login_required
@render_to('license.html')
def recordprint(request):
    return HttpResponseRedirect('/license/')


@login_required
@render_to('license.html')
def license(request):
    users = User.objects.filter(is_superuser=0).order_by('-date_joined')
    #users = User.objects.all()
 
    page = request.GET.get('page')
    result = []
    for us in users:
        licenselist = {}
        licenselist['license'] = us.username
        licenselist['password'] = us.user2.textpassword
        licenselist['status'] = us.user2.status
    #    print(us.username)
        result.append(licenselist)
    paginator = Paginator(result, 10)
    try:
        pgn = paginator.page(page)
    except PageNotAnInteger:
        pgn = paginator.page(1)
    except EmptyPage:
        pgn = paginator.page(paginator.num_pages)
 
    return {'licenses': pgn}
#===============================================================================
    

# Create your views here.
#@csrf_protect
def login(request, next='/'):
#def login(request):
    args = {}
    args.update(csrf(request))
    #next_page_get = request.GET.get('next', '/')
    #next_page_post = request.POST.get('next', '/')
    if request.POST:
        usr = request.POST.get('username', '')
        pwd = request.POST.get('password', '')
        print(usr)
        user = auth.authenticate(username=usr, password=pwd)
        print(user)
        if user is not None:
            if user.is_active:
                print(user.username)
                print(user.is_staff)
                auth.login(request, user)
                if user.is_staff:
                    return redirect("/license")
                else:
                    return redirect("/download")    
#            args['next'] = next_page_post
#            return redirect(next_page_post)
                
            else:
                args['login_error'] = 'The account has been disabled!'
                #args['next'] = next_page_post
                return render_to_response('login.html', args)
        else:
            args['login_error'] = 'User not found or incorrect password'
            #args['next'] = next_page_post
            return render_to_response('login.html', args)
    else:
        usr = request.GET.get('username', '')
        pwd = request.GET.get('password', '')
#        return render(request, "login.html", {'next': next_page_get})
        return render(request, "login.html")

def logout(request):
    auth.logout(request)
    return redirect("/")
