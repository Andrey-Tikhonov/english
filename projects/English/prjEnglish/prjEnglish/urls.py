"""prjEnglish URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.contrib import admin
from appEnglish import views as allviews
from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    url(r'^$', allviews.login, name='login'),
    url(r'^download/$', allviews.download, name='download'),
    url(r'^startdownload/$', allviews.startdownload, name='startdownload'),
    url(r'^license/record/add/$', allviews.recordadd, name='recordadd'),
    url(r'^license/record/info/$', allviews.recordinfo, name='recordinfo'),
    url(r'^license/record/print/$', allviews.recordprint, name='recordprint'),    
    url(r'^license/record/activate/(?P<lic>[\d\w]{8}-[\d\w]{4}-[\d\w]{4}-[\d\w]{4}-[\d\w]{12})', allviews.recordactivate, name='recordactivate'),
    url(r'^license/$', allviews.license, name='license'),
    url(r'^admin/', include(admin.site.urls)),
    url(r"^accounts/login", allviews.login, name='login'),
    url(r"^login", allviews.login, name='login'),
    url(r'^logout/$', allviews.logout, name='logout'),    
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# url(r'^admin/', admin.site.urls),
 



#urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#urlpatterns += staticfiles_urlpatterns()